Tutorial entirely based on https://testdriven.io/blog/deploying-django-to-heroku-with-docker/

### Test locally
- `docker build -t web:latest .`
Run the image passing also the environment variables:
- `docker run -d --name django-heroku -e "PORT=8765" -e "DEBUG=1" -p 8007:8765 web:latest`
Stop and remove the container once it's finished:
- `docker stop django-heroku`
- `docker rm django-heroku`

## Configure Heroku
Create app with:
- `heroku create`
In our case we have 'ancient-bastion-46428'.
Set environment variable with:
- `heroku config:set SECRET_KEY=SOME_SECRET_VALUE -a ancient-bastion-46428`
Add URL `ancient-bastion-46428.herokuapp.com` to list of ALLOWED host


- `docker build -t registry.heroku.com/ancient-bastion-46428/web .`

- `docker push registry.heroku.com/ancient-bastion-46428/web`
Release the image:
- `heroku container:release -a ancient-bastion-46428 web`

### Create the database:
- `heroku addons:create heroku-postgresql:hobby-dev -a ancient-bastion-46428`
Run the migrations:
- `heroku run python manage.py makemigrations -a ancient-bastion-46428`
- `heroku run python manage.py migrate -a ancient-bastion-46428`

- `heroku pg:psql -a ancient-bastion-46428`